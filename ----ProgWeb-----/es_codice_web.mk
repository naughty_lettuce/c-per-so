



<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->
[TOC]

#Web.xml


###Filtri

    <filter>
        <filter-name>AuthenticationFilter</filter-name>
        <filter-class>it.unitn.aa1617.webprogramming.lab07.todo.filters.AuthenticationFilter</filter-class>
    </filter>
    <filter-mapping>
        <filter-name>AuthenticationFilter</filter-name>
        <url-pattern>/restricted/*</url-pattern>
    </filter-mapping>

filter.java

	public class AuthenticationFilter implements Filter {

		private static final boolean debug = true;

		// The filter configuration object we are associated with.  If
		// this value is null, this filter instance is not currently
		// configured. 
		private FilterConfig filterConfig = null;

		public AuthenticationFilter() {    }

		private void doBeforeProcessing(ServletRequest request, ServletResponse response)
				throws IOException, ServletException {
			if (debug) {
				log("AuthenticationFilter:DoBeforeProcessing");
			}

			if (request instanceof HttpServletRequest) {
				ServletContext servletContext = ((HttpServletRequest) request).getServletContext();
				HttpSession session = ((HttpServletRequest) request).getSession(false);
				User user = null;
				if (session != null) {
					user = (User) session.getAttribute("user");
				}
				if (user == null) {
					String contextPath = servletContext.getContextPath();
					if (!contextPath.endsWith("/")) {
						contextPath += "/";
					}
					((HttpServletResponse) response).sendRedirect(((HttpServletResponse) response).encodeRedirectURL(contextPath + "login.handler"));
					return;
				}
			}
			//codice per "remember me"
		}

		private void doAfterProcessing(ServletRequest request, ServletResponse response)
				throws IOException, ServletException {
			if (debug) {
				log("AuthenticationFilter:DoAfterProcessing");
			}
		}

		public void doFilter(ServletRequest request, ServletResponse response,
				FilterChain chain)
				throws IOException, ServletException {

			if (debug) {
				log("AuthenticationFilter:doFilter()");
			}

			doBeforeProcessing(request, response);

			Throwable problem = null;
			try {
				chain.doFilter(request, response);
			} catch (Throwable t) {
				// If an exception is thrown somewhere down the filter chain,
				// we still want to execute our after processing, and then
				// rethrow the problem after that.
				problem = t;
				t.printStackTrace();
			}
			doAfterProcessing(request, response);

			// If there was a problem, we want to rethrow it if it is
			// a known type, otherwise log it.
			if (problem != null) {
				if (problem instanceof ServletException) {
					throw (ServletException) problem;
				}
				if (problem instanceof IOException) {
					throw (IOException) problem;
				}
				sendProcessingError(problem, response);
			}
		}
		public FilterConfig getFilterConfig() {
			return (this.filterConfig);
		}
		public void setFilterConfig(FilterConfig filterConfig) {
			this.filterConfig = filterConfig;
		}
		public void destroy() {
		}
		public void init(FilterConfig filterConfig) {
			this.filterConfig = filterConfig;
			if (filterConfig != null) {
				if (debug) {
					log("AuthenticationFilter:Initializing filter");
				}
			}
		}
		public String toString() {
			if (filterConfig == null) {
				return ("AuthenticationFilter()");
			}
			StringBuffer sb = new StringBuffer("AuthenticationFilter(");
			sb.append(filterConfig);
			sb.append(")");
			return (sb.toString());
		}
		private void sendProcessingError(Throwable t, ServletResponse response) {
			String stackTrace = getStackTrace(t);

			if (stackTrace != null && !stackTrace.equals("")) {
				try {
					response.setContentType("text/html");
					PrintStream ps = new PrintStream(response.getOutputStream());
					PrintWriter pw = new PrintWriter(ps);
					pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); //NOI18N

					// PENDING! Localize this for next official release
					pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
					pw.print(stackTrace);
					pw.print("</pre></body>\n</html>"); //NOI18N
					pw.close();
					ps.close();
					response.getOutputStream().close();
				} catch (Exception ex) {
				}
			} else {
				try {
					PrintStream ps = new PrintStream(response.getOutputStream());
					t.printStackTrace(ps);
					ps.close();
					response.getOutputStream().close();
				} catch (Exception ex) {
				}
			}
		}
		public static String getStackTrace(Throwable t) {
			String stackTrace = null;
			try {
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				t.printStackTrace(pw);
				pw.close();
				sw.close();
				stackTrace = sw.getBuffer().toString();
			} catch (Exception ex) {
			}
			return stackTrace;
		}

		public void log(String msg) {
			filterConfig.getServletContext().log(msg);
		}

	}


###Listeners

    <listener>
        <description>ServletContextListener</description>
        <listener-class>it.unitn.aa1617.webprogramming.lab07.todo.listeners.WebAppContextListener</listener-class>
    </listener>

listener.java

	public class WebAppContextListener implements ServletContextListener {
		@Override
		public void contextInitialized(ServletContextEvent sce) {
			String dburl = sce.getServletContext().getInitParameter("dburl");

			try {
				JDBCDAOFactory.configure(dburl);
				DAOFactory daoFactory = JDBCDAOFactory.getInstance();
				sce.getServletContext().setAttribute("daoFactory", daoFactory);
			} catch (DAOFactoryException ex) {
				Logger.getLogger(getClass().getName()).severe(ex.toString());
				throw new RuntimeException(ex);
			}
		}

		@Override
		public void contextDestroyed(ServletContextEvent sce) {
			DAOFactory daoFactory = (DAOFactory) sce.getServletContext().getAttribute("daoFactory");
			if (daoFactory != null) {
				daoFactory.shutdown();
			}
			daoFactory = null;
		}
	}


###Servlets

    <servlet>
        <servlet-name>LoginServlet</servlet-name>
        <servlet-class>it.unitn.aa1617.webprogramming.lab07.todo.servlets.LoginServlet</servlet-class>
    </servlet>
    <servlet-mapping>
        <servlet-name>LoginServlet</servlet-name>
        <url-pattern>/login.handler</url-pattern>
    </servlet-mapping>



###Session Timeout

	<session-config>
        <session-timeout>
            30
        </session-timeout>
    </session-config>

---
#DAO

###DAO (interfaccia)

	import pack.DAOException;
	import pack.DAOFactoryException;

	public interface DAO<ENTITY_CLASS, PRIMARY_KEY> {

		//ritorna il numero di record della tabella ENTITY_CLASS
		//in realtà magari ENTITY_CLASS=User, ma la tabella si chiama Utenti (si mette a posto dopo)
		public Long getCount() throws DAOException;
		
		//ritorna l'elemento della tabella ENTITY_CLASS con la chiave = PRIMARY_KEY
		public ENTITY_CLASS getByPrimaryKey(PRIMARY_KEY primaryKey) throws DAOException;

		//ritorna tutti gli elenti della tabella ENTITY_CLASS
		public List<ENTITY_CLASS> getAll() throws DAOException;

		//restituisce un DAO....
		public <DAO_CLASS extends DAO> DAO_CLASS getDAO(Class<DAO_CLASS> daoClass) throws DAOFactoryException;
	}

###ElementoDAO (interfaccia) (estende DAO)

	public interface ElementoDAO extends DAO<Elemento, Integer> {

		//funzione particolare che aggiunge questa estensione di dao
		public List<ToDo> getByColoreElemento(Integer colorId) throws DAOException;
	}

###JDBCDAO (abstract) (implementa DAO)

	public abstract class JDBCDAO<ENTITY_CLASS, PRIMARY_KEY_CLASS> implements DAO<ENTITY_CLASS, PRIMARY_KEY_CLASS> {
		protected final Connection CON;
		protected final HashMap<Class, DAO> FRIEND_DAOS; //lista di DAO con cui interagire

		protected JDBCDAO(Connection con) {
			super();
			this.CON = con;
			FRIEND_DAOS = new HashMap<>();
		}

		@override
		public <DAO_CLASS extends DAO> DAO_CLASS getDAO(Class<DAO_CLASS> daoClass) throws DAOFactoryException {
				return (DAO_CLASS) FRIEND_DAOS.get(daoClass);    
		}
	}

###JDBCElementoDAO (estende JDBCDAO)(implementa ElementDAO)(usbile, pratico)

	public class JDBCToDoDAO extends JDBCDAO<ToDo, Integer> implements ToDoDAO {

		public JDBCToDoDAO(Connection con) {
			super(con);
			FRIEND_DAOS.put(UserDAO.class, new JDBCUserDAO(CON));
		}

		public Long getCount() throws DAOException {
			try (PreparedStatement stmt = CON.prepareStatement("SELECT COUNT(*) FROM elemento");) {
				ResultSet counter = stmt.executeQuery();
				if (counter.next()) {
					return counter.getLong(1);
				}
			} catch (SQLException ex) {
				throw new DAOException("Impossible to count todos", ex);
			}

			return 0L;
		}

		public ToDo getByPrimaryKey(Integer primaryKey) throws DAOException {
			if (primaryKey == null) {
				throw new DAOException("primaryKey is null");
			}
			try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM todos WHERE id = ?")) {
				stm.setInt(1, primaryKey);
				try (ResultSet rs = stm.executeQuery()) {

					rs.next();
					ToDo todo = new ToDo();
					todo.setId(rs.getInt("id"));
					todo.setDescription(rs.getString("description"));
					todo.setLocation(rs.getString("location"));
					Timestamp timestamp = rs.getTimestamp("todo_date");
					todo.setTimestamp(new Date(timestamp.getTime()));

					UserDAO userDao = getDAO(UserDAO.class);
					todo.setUser(userDao.getByPrimaryKey(rs.getInt("id_user")));

					return todo;
				}
			} catch (SQLException | DAOFactoryException ex) {
				throw new DAOException("Impossible to get the to-do for the passed primary key", ex);
			}
		}




		public List<ToDo> getAll() throws DAOException {
			List<ToDo> todos = new ArrayList<>();

			try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM todos ORDER BY todo_date")) {
				try (ResultSet rs = stm.executeQuery()) {

					while (rs.next()) {
						ToDo todo = new ToDo();
						todo.setId(rs.getInt("id"));
						todo.setDescription(rs.getString("description"));
						todo.setLocation(rs.getString("location"));
						Timestamp timestamp = rs.getTimestamp("todo_date");
						todo.setTimestamp(new Date(timestamp.getTime()));

						UserDAO userDao = getDAO(UserDAO.class);
						todo.setUser(userDao.getByPrimaryKey(rs.getInt("id_user")));

						todos.add(todo);
					}
				}
			} catch (SQLException | DAOFactoryException ex) {
				throw new DAOException("Impossible to get the list of to-dos", ex);
			}

			return todos;
		}


		public Long insert(ToDo todo) throws DAOException {
			try (PreparedStatement ps = CON.prepareStatement("INSERT INTO Todos(description, location, todo_date, id_user) VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS)) {
				
				ps.setString(1, todo.getDescription());
				ps.setString(2, todo.getLocation());
				ps.setTimestamp(3, new Timestamp(todo.getTimestamp().getTime()));
				ps.setInt(4, todo.getUser().getId());

				if (ps.executeUpdate() == 1) {
					ResultSet generatedKeys = ps.getGeneratedKeys();
					if (generatedKeys.next()) {
						return generatedKeys.getLong(1);
					} else {
						try {
							CON.rollback();
						} catch (SQLException ex) {
							//TODO: log the exception
						}
						throw new DAOException("Impossible to persist the new to-do");
					}
				} else {
					try {
						CON.rollback();
					} catch (SQLException ex) {
						//TODO: log the exception
					}
					throw new DAOException("Impossible to persist the new to-do");
				}
			} catch (SQLException ex) {
				try {
					CON.rollback();
				} catch (SQLException ex1) {
					//TODO: log the exception
				}
				throw new DAOException("Impossible to persist the new to-do", ex);
			}
		}



	public List<ToDo> getByUserId(Integer userId) throws DAOException {
			if (userId == null) {
				throw new DAOException("userId is null");
			}

			List<ToDo> todos = new ArrayList<>();

			try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM todos WHERE id_user = ? ORDER BY todo_date")) {
				stm.setInt(1, userId);
				try (ResultSet rs = stm.executeQuery()) {

					while (rs.next()) {
						ToDo todo = new ToDo();
						todo.setId(rs.getInt("id"));
						todo.setDescription(rs.getString("description"));
						todo.setLocation(rs.getString("location"));
						Timestamp timestamp = rs.getTimestamp("todo_date");
						todo.setTimestamp(new Date(timestamp.getTime()));

						try {
							UserDAO userDao = getDAO(UserDAO.class);
							todo.setUser(userDao.getByPrimaryKey(rs.getInt("id_user")));
							todos.add(todo);
						} catch (DAOFactoryException ex) {
							throw new DAOException("Impossible to get the list of to-dos", ex);
						}
					}
				}
			} catch (SQLException ex) {
				throw new DAOException("Impossible to retrieve the list of to-dos", ex);
			}

			return todos;
		}
	}

### Entità

	public class User{
		//i dao e le varie classi di passano oggetti che appartengono al package entità
		String nome; String cognome; String email; String password; int età; 
	}

###DAOExeption

	public class DAOException extends Exception {
		public DAOException() {
			super();
		}
		public DAOException(String message) {
			super(message);
		}
		public DAOException(Throwable cause) {
			super(cause);
		}
		public DAOException(String message, Throwable cause) {
			super(message, cause);
		}
	}

###DAOFactoryException

	public class DAOFactoryException extends Exception {
		public DAOFactoryException() {
			super();
		}
		public DAOFactoryException(String message) {
			super(message);
		}
		public DAOFactoryException(Throwable cause) {
			super(cause);
		}
		public DAOFactoryException(String message, Throwable cause) {
			super(message, cause);
		}
	}

###DAOFactory (interfaccia)

	import pack.DAO
	import pack.DAOFactoryException

	public interface DAOFactory {

		//spegne il DAO
		public void shutdown();

		//parametro una classe, ritorna il DAO che gestisce quella classe
		public <DAO_CLASS extends DAO> DAO_CLASS getDAO(Class<DAO_CLASS> daoInterface) throws DAOFactoryException;
	}

###JDBCDAOFactory (implementa DAOFactory)

public class JDBCDAOFactory implements DAOFactory {

    private final transient Connection CON;
    private final transient HashMap<Class, DAO> DAO_CACHE;

	private static JDBCDAOFactory instance;

	//mette in istance un nuovo JDBCDAOFactory, è unico per tutte le classi (static)
	public static void configure(String dbUrl) throws DAOFactoryException {
        if (instance == null) {
            instance = new JDBCDAOFactory(dbUrl);
        } else {
            throw new DAOFactoryException("DAOFactory already configured. You can call configure only one time");
        }
    }

	//ritorna il JDBCDAOFactory statico
	public static JDBCDAOFactory getInstance() throws DAOFactoryException {
        if (instance == null) {
            throw new DAOFactoryException("DAOFactory not yet configured. Call DAOFactory.configure(String dbUrl) before use the class");
        }
        return instance;
    }

	//carica driver, prova a connetersi, inizializza hash
	private JDBCDAOFactory(String dbUrl) throws DAOFactoryException {
        super();

        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver", true, getClass().getClassLoader());
        } catch (ClassNotFoundException cnfe) {
            throw new RuntimeException(cnfe.getMessage(), cnfe.getCause());
        }

        try {
            CON = DriverManager.getConnection(dbUrl);
        } catch (SQLException sqle) {
            throw new DAOFactoryException("Cannot create connection", sqle);
        }
        
        DAO_CACHE = new HashMap<>();
    }

	//spegne qualcosa
	public void shutdown() {
        try {
            DriverManager.getConnection("jdbc:derby:;shutdown=true");
        } catch (SQLException sqle) {
            Logger.getLogger(JDBCDAOFactory.class.getName()).info(sqle.getMessage());
        }
    }

	//prova a creare un JDBC+DAO_CLASS e lo ritorna
	public <DAO_CLASS extends DAO> DAO_CLASS getDAO(Class<DAO_CLASS> daoInterface) throws DAOFactoryException {
        DAO dao = DAO_CACHE.get(daoInterface);
        if (dao != null) {
            return (DAO_CLASS) dao;
        }
        
        Package pkg = daoInterface.getPackage();
        String prefix = pkg.getName() + ".jdbc.JDBC";
        
        try {
            Class daoClass = Class.forName(prefix + daoInterface.getSimpleName());
            
            Constructor<DAO_CLASS> constructor = daoClass.getConstructor(Connection.class);
            DAO_CLASS instance = constructor.newInstance(CON);
            if (!(instance instanceof JDBCDAO)) {
                throw new DAOFactoryException("The daoInterface passed as parameter doesn't extend JDBCDAO class");
            }
            DAO_CACHE.put(daoClass, instance);
            return instance;
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException | SecurityException ex) {
            throw new DAOFactoryException("Impossible to return the DAO", ex);
        }
    }
}




