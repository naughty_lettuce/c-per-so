
M?=standardcomment

.PHONY: git_update, git_push

git_push: git_update
	git push https://thomas.reolon%40studenti.unitn.it:CANEpane11!@gitlab.com/naughty_lettuce/c-per-so.git
	@echo "-----------------------------------------"

git_update: 
	git add --all
	git commit -m "$(M)"
	@echo "-----------------------------------------"
	
