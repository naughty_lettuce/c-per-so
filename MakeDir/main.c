#include <stdlib.h>
#include <stdio.h>
#include "cifratore.h"


int main(int argc, char* argv[]){
    char str[2000], c, clear, *res;

    printf("type 0 for cifrare, 1 for decifrare\n");
    scanf("%c", &c);
    while ((clear = getchar()) != '\n' && clear != EOF) { }



    if (c=='0')
        printf("scrivi la frase da cifrare:\n");
    else
        printf("scrivi la frase da decifrare:\n");
    scanf("%[^\n]", str);



    switch(c){
        case '0': res = cif_cripta(str);break;
        case '1': res = cif_decripta(str);break;
        default: exit(1);
    }

    printf("%s\n", res);

    /*https://github.com/roxma/easymake/blob/master/easymake.mk*/
    return 0;
}

