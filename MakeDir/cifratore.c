
#ifndef STRING_H
#define STRING_H

#include <string.h>

#endif

#ifndef STDLIB_H
#define STDLIB_H

#include <stdlib.h>

#endif

#ifndef STDIO_H
#define STDIO_H

#include <stdio.h>

#endif

#include "cifratore.h"

char* cif_cripta(char s[]){
    char* ss = (char*) malloc(sizeof(char)*2000);

    int l = strlen(s);
    char x = s[l/3], y=s[l/2];

    for (int i=0; i<l; i++){
        if (i == l/3 || i == l/2)
            ss[i] = s[i];
        else if (i%5==3)
            ss[i] = s[i] + (x%10);
        else if (i%2==0 && i<l/3)
            ss[i] = s[i] + (y%9);
        else
            ss[i] = s[i] + (x%3) + (y%5);

        //printf("i->%d\t %c -> %c\n", i, s[i], ss[i]);
    }
    ss[l] = '\0';

    return ss;
}

char* cif_decripta(char s[]){
    char* ss = (char*) malloc(sizeof(char)*2000);

    int l = strlen(s);
    char x = s[l/3], y=s[l/2];

    for (int i=0; i<l; i++){
        if (i == l/3 || i==l/2)
            ss[i] = s[i];
        else if (i%5==3)
            ss[i] = s[i] - (x%10);
        else if (i%2==0 && i<l/3)
            ss[i] = s[i] - (y%9);
        else
            ss[i] = s[i] - (x%3) - (y%5);
    }
    ss[l] = '\0';

    return ss;
}

