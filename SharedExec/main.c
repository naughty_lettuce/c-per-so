
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/wait.h>

/*
* il programma crea 2 segmenti di memoria condivisa, uno con shmget, l'altro con mmap
* dopo un fork il figlio chiama un exec (./son), che scrive all' interno della memoria condivisa
* infine il padre legge cosa ha scritto il figlio
*/
int main(){
	int shm0, fd1, fid;
	char *memo0, *memo1;


	//printf("creat:%d, irusr:%d, iwusr:%d", IPC_CREAT, S_IRUSR, S_IWUSR);

	//shmget
	if ( //crea l'associazione 88888 <-> pezzo memoria condivisa scrivibile
	(shm0 = shmget(88888, sizeof(char)*100, IPC_CREAT|S_IRUSR|S_IWUSR)) == -1)
		{perror("shmget");exit(1);}
	
	if ( //restituisce un indirizzo che fa riferimento al pezzo di memoria 88888
	(memo0 = shmat (shm0, NULL, 0)) == (void*)-1)
		{perror("shmat");exit(1);}


	//mmap
	if (  //crea nel kernel una associazione: "/memoria1" <-> pezzo memoria condivisa scrivibile
	(fd1 = shm_open("/memoria1", O_CREAT|O_RDWR, S_IRUSR|S_IWUSR)) == -1)
		{perror("open");exit(1);}

	if ( //setta la grandezza del pezzo di memoria
	(ftruncate(fd1, sizeof(char)*100)) == -1)
		{perror("trunc");exit(1);}

	if ( //restituisce un indirizzo che fa riferimento al pezzo di memoria /memoria1
    (memo1 = mmap(NULL, sizeof(char)*100, PROT_READ|PROT_WRITE, MAP_SHARED, fd1, 0)) == (void*)-1 )
        {perror("mmap"); exit(1);}
	

	//fork
	if (
	(fid=fork())==-1)
		{perror("fork"); exit(1);}

	if (fid==0){
		close(fd1); 
		shmdt(memo0);
		if (execl("./son", "./son", NULL) == -1) //problema ad indicare la path
			{perror("execl"); exit(0);}
	}else{
		wait(NULL);
		printf("-shmget->[%s]\n", memo0);
		printf("--mmap-->[%s]\n", memo1);

		close(fd1);
		shmctl(shm0,IPC_RMID,0); //dealloca memoria

		shmdt(memo0);
		shm_unlink("/memoria1"); //dealloca memoria
	}
	return 0;
}