#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <string.h>


int main(){

	int shm0, fd1;
	char *memo0, *memo1;

	//shmget
    if (
	(shm0 = shmget(88888, sizeof(char)*100, S_IRUSR|S_IWUSR)) == -1)
		{perror("son-shmget");exit(1);}
	
	if (
	(memo0 = shmat (shm0, NULL, 0)) == (void*)-1)
		{perror("son-shmat");exit(1);}

    strcpy(memo0, "Cu spatti avi a megghiu parti");
	shmdt(memo0);

	//mmap
	if (
	(fd1 = shm_open("/memoria1", O_RDWR, S_IRUSR|S_IWUSR)) == -1)
		{perror("son-open");exit(1);}

	if (
	(ftruncate(fd1, sizeof(char)*100)) == -1)
		{perror("son-trunc");exit(1);}

	if (
        (memo1 = mmap(NULL, sizeof(char)*100, PROT_READ|PROT_WRITE, MAP_SHARED, fd1, 0)) == (void*)-1 )
        {perror("son-mmap"); exit(1);}
	
    strcpy(memo1, "Cu `un fa nenti `un sbaglia nenti");
   	close(fd1);

    
	return 0;
}